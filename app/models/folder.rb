class Folder < ApplicationRecord
  has_many :sub_folders, class_name: 'Folder', foreign_key: :parent_id
  belongs_to :super_folder, class_name: 'Folder', foreign_key: :parent_id, required: false

  scope :roots, -> { where(parent_id: nil) }
  validates :name, uniqueness: true, presence:true
end
