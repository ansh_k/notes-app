class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]

  # GET /notes
  def index
    @notes = Note.all
  end

  # GET /notes/1
  def show
  end

  # GET /notes/1/edit
  def edit
  end

  # POST /notes
  def create
    @note = Note.new(note_params)

    if @note.save
      path = note_params[:folder_id].present? ? Folder.find(note_params[:folder_id]) : root_path
      flash[:notice] = 'Note was successfully created.'
      redirect_to path
    else
      flash[:error] = @note.errors.full_messages[0]
      redirect_to root_path
    end
  end

  # PATCH/PUT /notes/1
  def update
    if @note.update(note_params)
      flash[:notice] = 'Note was successfully updated.'
      redirect_to @note
    else
      flash[:error] = @note.errors.full_messages[0]
      render :edit
    end
  end

  # DELETE /notes/1
  def destroy
    path = @note.folder_id.present? ? Folder.find(@note.folder_id) : root_path
    @note.destroy
    flash[:notice] = 'Note was successfully destroyed.'
    redirect_to path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def note_params
      params.permit(:title, :content, :folder_id)
    end
end
