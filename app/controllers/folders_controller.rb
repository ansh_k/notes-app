class FoldersController < ApplicationController
  before_action :set_folder, only: [:show, :destroy]

  # GET /folders
  def index
    @folders = Folder.where(parent_id: nil)
    @notes = Note.where(folder_id: nil)
  end

  # GET /folders/1
  def show
    @sub_folders = @folder.sub_folders
    @notes = Note.where(folder_id: @folder.id)
    get_super_folder_info
  end

  # POST /folders
  def create
    @folder = Folder.new(folder_params)
    if @folder.save
      path = folder_params[:parent_id].present? ? Folder.find(folder_params[:parent_id]) : root_path
      flash[:notice] = 'Folder was successfully created.'
      redirect_to path and return
    else
      flash[:error] = @folder.errors.full_messages[0]
      redirect_to root_path
    end
  end

  # DELETE /folders/1
  def destroy
    path = @folder.parent_id.present? ? Folder.find(@folder.parent_id) : root_path
    @folder.destroy
    flash[:notice] = 'Folder was successfully destroyed.'
    redirect_to path
  end

  # GET /search_folder
  def search_folder_and_subfolders
    @folder = Folder.find_by(name: params[:name]) if params[:name].present?
    path = msg = ''
    if @folder.present?
      path =  @folder
      flash[:notice] = 'Searched successfully'
    else
      path = root_path
      flash[:error] = "Don't have any folder or subfolders with given name"
    end
    redirect_to path
  end

  private
    # Use to get super folder information like id and name
    def get_super_folder_info
      folder = @folder 
      @sup_folders_names = []
      @sup_folders_ids = []
      while folder.super_folder.present? 
        @sup_folders_names.push(folder.super_folder.name)
        @sup_folders_ids.push(folder.super_folder.id)
        folder = folder.super_folder 
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_folder
      @folder = Folder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def folder_params
      params.permit(:name, :parent_id)
    end
end
