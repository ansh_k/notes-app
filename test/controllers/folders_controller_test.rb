require 'test_helper'

class FoldersControllerTest < ActionDispatch::IntegrationTest
  setup do 
    @folder18 = folders(:folder18)
  end

  test "should search folder from root" do
    folder1 = folders(:folder1)
    get search_folder_url, params: { name: 'folder1' }
    assert_redirected_to folder_path(folder1)
    assert_equal 'Searched successfully', flash[:notice]
  end

  test "should search folder which has one parent" do
    folder4 = folders(:folder4)
    get search_folder_url, params: { name: 'folder4' }
    assert_redirected_to folder_path(folder4)
    assert_equal 'Searched successfully', flash[:notice]
  end

  test "should redirect to root if folder has not found" do
    get search_folder_url, params: { name: 'folders' }
    assert_redirected_to root_url
    assert_equal "Don't have any folder or subfolders with given name", flash[:error]
  end

  test "should search folder from 15 folders deep" do
    get search_folder_url, params: { name: 'folder18' }
    folder = @folder18
    count = 0
    while folder.super_folder.present?
      count = count + 1
      folder = folder.super_folder 
    end
    assert_equal(15, count, msg = nil)
    assert_redirected_to "/folders/#{@folder18.id}"
    assert_equal 'Searched successfully', flash[:notice]
  end

  test "should contain 50+ notes from 15 folders deep" do
    get folder_url(@folder18)
    count = 0
    count = Note.where(folder_id: @folder18.id).count
    assert_operator 50, :<=, count
  end
end
