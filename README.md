# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  '2.6.3'

* Configuration
  1. git clone https://ansh_k@bitbucket.org/ansh_k/notes-app.git
  2. bundle install

* Database creation
  rake db:create

* Database initialization
  rake db:migrate

* How to run the test suite
  rake test