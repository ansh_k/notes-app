Rails.application.routes.draw do
  root to: 'folders#index'
  
  resources :notes, only: [:show, :edit, :update, :destroy, :create]
  resources :folders, only: [:show, :destroy, :create]

  get '/search_folder', to: 'folders#search_folder_and_subfolders'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
